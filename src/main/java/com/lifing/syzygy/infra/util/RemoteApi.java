package com.lifing.syzygy.infra.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;

@Component
public class RemoteApi {

	public static Object execute(String targetURL, Object obj, String requestMethod, String token) {

		HttpURLConnection connection = null;

		try {
			// convert object to Json String
			Gson gson = new Gson();
			String json = gson.toJson(obj);
			// bytes to pass over the network
			byte[] postData = json.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;

			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			// Set URL and and content details
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			// connection.setRequestMethod(requestMethod);
			if (requestMethod.equals("PUT")) {
				connection.setRequestMethod(requestMethod);
			}
			if (requestMethod.equals("PATCH")) {
				connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				connection.setRequestMethod("POST");
			}
			connection.setRequestProperty("X-Access-Token", token);

			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			connection.setUseCaches(false);
			if (!requestMethod.equals("GET")) {

				try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
					wr.write(postData);
				}
			}
			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}

			return Document.parse(sb.toString());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static Object execute(String targetURL, Object obj, String requestMethod) {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HttpURLConnection connection = null;

		try {
			// convert object to Json String
			Gson gson = new Gson();
			String json = gson.toJson(obj);
			// bytes to pass over the network
			byte[] postData = json.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;

			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			// Set URL and and content details
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			// connection.setRequestMethod(requestMethod);
			if (requestMethod.equals("PUT")) {
				connection.setRequestMethod(requestMethod);
			}
			if (requestMethod.equals("PATCH")) {
				connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				connection.setRequestMethod("POST");
			}
			if ("" != request.getHeader("X-Access-Token") || null != request.getHeader("X-Access-Token")
					|| !request.getHeader("X-Access-Token").equals(null)) {
				connection.setRequestProperty("X-Access-Token", request.getHeader("X-Access-Token"));
			}
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			connection.setUseCaches(false);
			if (!requestMethod.equals("GET")) {

				try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
					wr.write(postData);
				}
			}
			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}

			return Document.parse(sb.toString());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/***
	 * This method should be used when we are Asynch processes. obj should have x
	 * access token.
	 * 
	 * @param targetURL
	 * @param obj
	 * @param requestMethod
	 * @return
	 */
	public static Object invokeApi(String targetURL, Map<String, Object> obj, String requestMethod) {

		HttpURLConnection connection = null;

		try {
			// convert object to Json String
			Gson gson = new Gson();
			String json = gson.toJson(obj);
			// bytes to pass over the network
			byte[] postData = json.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;

			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			// Set URL and and content details
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			// connection.setRequestMethod(requestMethod);
			if (requestMethod.equals("PUT")) {
				connection.setRequestMethod(requestMethod);
			}
			if (requestMethod.equals("PATCH")) {
				connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
				connection.setRequestMethod("POST");
			}
			if ("" != obj.get("xAcessToken")) {
				connection.setRequestProperty("X-Access-Token", obj.get("xAcessToken").toString());
			}
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			connection.setUseCaches(false);
			if (!requestMethod.equals("GET")) {

				try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
					wr.write(postData);
				}
			}
			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}

			return Document.parse(sb.toString());

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static List<?> execute(String targetURL, Map<String, Object> obj) {

		HttpURLConnection connection = null;

		try {

			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			// Set URL and and content details
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			// connection.setRequestMethod(requestMethod);

			if ("" != obj.get("xAcessToken")) {
				connection.setRequestProperty("X-Access-Token", obj.get("xAcessToken").toString());
			}
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("charset", "utf-8");

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			return new com.google.gson.Gson().fromJson(rd, List.class);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	public static List<?> execute(String targetURL) {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HttpURLConnection connection = null;

		try {

			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();

			// Set URL and and content details
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			// connection.setRequestMethod(requestMethod);

			if ("" != request.getHeader("X-Access-Token") || null != request.getHeader("X-Access-Token")
					|| !request.getHeader("X-Access-Token").equals(null)) {
				connection.setRequestProperty("X-Access-Token", request.getHeader("X-Access-Token"));
			}
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			connection.setRequestProperty("charset", "utf-8");

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			return new com.google.gson.Gson().fromJson(rd, List.class);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
