package com.lifing.syzygy.infra.util;

import java.util.LinkedHashMap;

import org.springframework.context.annotation.Scope;


@Scope("session")
public class SessionScope {

	public static LinkedHashMap<Object, Object> currentUser = new LinkedHashMap<Object, Object>();
}
