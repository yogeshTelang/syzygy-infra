package com.lifing.syzygy.infra.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.stereotype.Component;

import org.bson.Document;

@Component
public class SyUtil {
	public Object cleanup(Document obj) {
		obj.remove("_id");
		obj.remove("_class");
		return obj;
	}

	public Object cleanupBusinessProfile(Document obj) {
		obj.remove("_id");
		obj.remove("_class");
		obj.remove("countryCode");
		obj.remove("pricing");
		obj.remove("mobile");
		obj.remove("payFeature");
		obj.remove("X-Acess-Token");
		obj.remove("requestURL");
		obj.remove("methodName");
		obj.remove("jwtToken");
		obj.remove("remoteHost");
		obj.remove("remoteAddress");
		obj.remove("status");
		obj.remove("authId");
		obj.remove("lastLoginDateTime");
		obj.remove("jwtRefreshToken");
		obj.remove("fcmRegistrationId");
		obj.remove("organizationVideo");
		obj.remove("auditTime");
		obj.remove("queryString");
		obj.remove("createdDate");
		obj.remove("businessProfileId");
		obj.remove("updatedDateTime");
		obj.remove("createdBy");
		obj.remove("discount");
		obj.remove("updatedBy");
		obj.remove("version");
		return obj;
	}

	public Object deepClean(Document obj) {
		obj.remove("_id");
		obj.remove("_class");
		obj.remove("createdDateTime");
		obj.remove("updatedDateTime");
		obj.remove("createdBy");
		obj.remove("updatedBy");
		obj.remove("avatar");
		return obj;
	}

	public Object cleanupAspirantForNonAuth(Document obj) {
		obj.remove("_id");
		obj.remove("askFirstJobPreference");
		obj.remove("authId");
		obj.remove("chatId");
		obj.remove("countryCode");
		obj.remove("email");
		obj.remove("fcmRegistrationId");
		obj.remove("fcmUpdate");
		obj.remove("firstName");
		obj.remove("lastName");
		obj.remove("phoneNumber");
		obj.remove("preferredName");
		obj.remove("status");
		obj.remove("updatedBy");
		obj.remove("userDOB");
		obj.remove("user_details");
		obj.remove("updatedDateTime");
		obj.remove("education");
		obj.remove("version");
		return obj;
	}

	/**
	 * Fetch the city from location
	 * 
	 * @param aadress, Example: 1. "address": "Kannada Research Institute, Karnatak
	 *                 University Campus, Dharwad, Karnataka 580003, India"
	 * 
	 *                 2. "address": "KUD Rd, Karnatak University Campus, Dharwad,
	 *                 Karnataka 580003, India"
	 * 
	 *                 3. "address": "Karnatak University Campus,Hubali-Dharwad,
	 *                 Karnataka 580003, India"
	 * 
	 *                 4. "address": "Hubali-Dharwad, Karnataka, India"
	 * 
	 *                 5. "address": "Hubali-Dharwad, Karnataka 580003, India"
	 * 
	 *                 6. "address": "Karnataka, India"
	 * @return
	 */
	public String getCity(String address) {
		try {
			List<String> location = Arrays.asList(address.split(","));
			return location.get(location.size() - 3);
		} catch (Exception e) {
			return address;
		}
	}

	/**
	 * Fetch the country from location
	 * 
	 * @param aadress, Example: 1. "address": "Kannada Research Institute, Karnatak
	 *                 University Campus, Dharwad, Karnataka 580003, India"
	 * 
	 *                 2. "address": "KUD Rd, Karnatak University Campus, Dharwad,
	 *                 Karnataka 580003, India"
	 * 
	 *                 3. "address": "Karnatak University Campus,Hubali-Dharwad,
	 *                 Karnataka 580003, India"
	 * 
	 *                 4. "address": "Hubali-Dharwad, Karnataka, India"
	 * 
	 *                 5. "address": "Hubali-Dharwad, Karnataka 580003, India"
	 * 
	 *                 6. "address": "Karnataka, India"
	 * @return
	 */
	public String getCountry(String address) {
		try {
			List<String> location = Arrays.asList(address.split(","));
			return location.get(location.size() - 1);
		} catch (Exception e) {
			return address;
		}
	}

	/**
	 * calculate distance between two LatLngs
	 * 
	 * @param StartP
	 * @param EndP
	 * @return
	 */
	public double calculateDistance(double startLat, double startLng, double endLat, double endLng) {
		int radius = 6371;// radius of earth in Km
		double dLat = Math.toRadians(endLat - startLat);
		double dLon = Math.toRadians(endLng - startLng);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(startLat))
				* Math.cos(Math.toRadians(endLat)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		double c = 2 * Math.asin(Math.sqrt(a));
		double valueResult = radius * c;
		double km = valueResult / 1;
		DecimalFormat newFormat = new DecimalFormat("####");
		int kmInDec = Integer.parseInt(newFormat.format(km));
		double meter = valueResult % 1000;
		int meterInDec = Integer.parseInt(newFormat.format(meter));
		System.out.println("calculateDistance " + valueResult + "   KM  " + kmInDec + " Meter   " + meterInDec);
		// return in kms
		return radius * c;

	}

	/**
	 * This method is used for converting any string to SEO compatible.
	 * 
	 * @param string
	 * @return
	 */

	public String getSeoString(String string) {
		try {
			return string.replaceAll(" ", "-").toLowerCase();
		} catch (Exception e) {
			return string;
		}
	}

	/**
	 * Get the day, Month and year in the format Monday 10, September 2018
	 * 
	 * @param dateString
	 * @return
	 */
	public String getDayAndMonth(String dateString) {

		try {
			String date[] = dateString.split("T");

			LocalDate localDate = LocalDate.parse(date[0]);
			DayOfWeek dayOfWeek = localDate.getDayOfWeek();
			String day = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
			String dateNumber[] = date[0].split("-");

			if (dateNumber[2].equals("1") || dateNumber[2] == "1" || dateNumber[2].equals("21") || dateNumber[2] == "21"
					|| dateNumber[2].equals("31") || dateNumber[2] == "31")
				dateNumber[2] = dateNumber[2] + "st";
			else if (dateNumber[2].equals("2") || dateNumber[2] == "2" || dateNumber[2].equals("22")
					|| dateNumber[2] == "22")
				dateNumber[2] = dateNumber[2] + "nd";
			else if (dateNumber[2].equals("3") || dateNumber[2] == "3" || dateNumber[2].equals("23")
					|| dateNumber[2] == "23")
				dateNumber[2] = dateNumber[2] + "rd";
			else
				dateNumber[2] = dateNumber[2] + "th";
			day = day + " " + dateNumber[2]; // Monday 10th

			String actualDate = date[0];
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
			DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MMM yyyy", Locale.ENGLISH);
			LocalDate ld = LocalDate.parse(actualDate, dtf);
			String monthName = dtf2.format(ld); // September 2018

			return day + ", " + monthName;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param compareDate, the present day gig
	 * @param date1,       startDate of the existing gig
	 * @param date2,       endDate of the existing gig
	 * @return
	 */
	public String getBtwDates(String compareDate, String date1, String date2) {

		try {
			String compareDates[] = compareDate.split("T");
			String date1s[] = date1.split("T");
			String date2s[] = date2.split("T");

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			Date presentDate = dateFormat.parse(compareDates[0]);
			Date fromDate = dateFormat.parse(date1s[0]);
			Date toDate = dateFormat.parse(date2s[0]);

			if (presentDate.compareTo(fromDate) == 0 || presentDate.compareTo(toDate) == 0
					|| (presentDate.after(fromDate) && presentDate.before(toDate))
					|| (presentDate.before(fromDate) && presentDate.before(toDate))
					|| (presentDate.before(fromDate) && presentDate.after(toDate))) {
				return "SAME_DAY_GIG";
			} else {
				return "NOT_A_SAME_DAY_GIG";
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return "NOT_A_SAME_DAY_GIG";

		}

	}

}
