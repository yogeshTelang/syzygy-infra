package com.lifing.syzygy.infra.file.operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

@SuppressWarnings("unchecked")
@Component
public class CSVGenerator {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public void generate(Map<String, Object> params, String action, HttpServletRequest request,
			HttpServletResponse response) {

		if (action.contentEquals("CSV")) {
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + action + ".csv\"");
		} else if (action.contentEquals("PDF")) {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + action + ".pdf\"");
		} else if (action.contentEquals("IMAGE")) {
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + action + ".png\"");
		}

		response.setCharacterEncoding("UTF-8");

		List<Student> students = new ArrayList<Student>();
		List<Document> users = (List<Document>) params.get("appliedAspirants");

		for (Document user : users) {

			try {
				Student stdnt = new Student();
				Document profile = new Document((Map<String, Object>) user.get("profile"));
				stdnt.setFullName(profile.get("fullName").toString());
				stdnt.setPhoneNumber(profile.get("phoneNumber").toString());
				stdnt.setEmail(profile.get("email").toString());
				stdnt.setCity(profile.get("city").toString());
				// added try catch because location is not mandatory in android app and this was
				// throwing nullPointer and that particular student was not getting added
				try {
					stdnt.setLocation((new Document((Map<String, Object>)profile.get("location")).get("name").toString()));
				} catch (Exception e) {
					stdnt.setLocation("No data");
				}
				stdnt.setCollege((new Document((Map<String, Object>)profile.get("education")).get("college").toString()));
				stdnt.setGraduatingYear(( new Document((Map<String, Object>) profile.get("education")).get("graduationYear").toString()));
				stdnt.setGender(profile.get("userGender").toString());
				stdnt.setAboutMe(profile.get("careerAspiration").toString());
				stdnt.setHashtags(profile.get("userHashtags").toString());
				stdnt.setAppliedDate(user.get("createdDateTime").toString());

				students.add(stdnt);
			} catch (Exception e) {
				System.out.println(e);
				e.getMessage();
			}

		}
		String[] header = { "FullName", "PhoneNumber", "Email", "City", "Location", "College", "GraduatingYear",
				"Gender", "AboutMe", "Hashtags", "AppliedDate" };
		ICsvBeanWriter csvWriter;
		try {
			csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
			csvWriter.writeHeader(header);

			for (Student user : students) {
				csvWriter.write(user, header);
			}

			csvWriter.close();
		} catch (IOException e) {
			log.error("SyCommonRepository.generate(params,action,request,response)", params.size(),action, e);

		}
	}
}
