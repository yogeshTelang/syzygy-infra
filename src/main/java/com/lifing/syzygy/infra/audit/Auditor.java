package com.lifing.syzygy.infra.audit;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lifing.syzygy.infra.util.SessionScope;

public class Auditor {

	/**
	 * Set the audit details for any collection, when saving first time
	 * 
	 * @param doc
	 * @return
	 */
	public static Document setAudit(Document doc) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			AuditEntity audit = new AuditEntity();
			Document authProfile = (Document) (SessionScope.currentUser.get(request.getHeader("X-Access-Token")));
			audit.setCreatedDate(new Date());
			audit.setLastModifiedDate(new Date());
			audit.setVersion(0L);
			doc.put("createdDateTime", audit.getCreatedDate());
			doc.put("updatedDateTime", audit.getLastModifiedDate());

			if (authProfile != null) {
				doc.put("createdBy", authProfile.get("fullName"));
				doc.put("updatedBy", authProfile.get("fullName"));
				if (doc.get("avatar") != null) {
					doc.put("avatar", authProfile.get("avatar"));
				}
			}

			doc.put("version", audit.getVersion());
			return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return doc;
		}
	}

	/**
	 * Update the audit details for any collection
	 * 
	 * @param doc
	 * @return
	 */
	public static Document updateAudit(Document doc) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			AuditEntity audit = new AuditEntity();
			Document authProfile = (Document) (SessionScope.currentUser.get(request.getHeader("X-Access-Token")));
			audit.setLastModifiedDate(new Date());

			try {
				doc.put("createdDateTime", new Date(doc.getLong("createdDateTime")));
			} catch (Exception e) {
				e.getMessage();
			}
			doc.put("updatedDateTime", audit.getLastModifiedDate());
			doc.put("updatedBy", authProfile.get("fullName"));
			if (doc.containsKey("version")) {
				doc.put("version", doc.get("version").toString() == null ? 0
						: Integer.parseInt(doc.get("version").toString()) + 1);
			} else {
				doc.put("version", 1);
			}
			return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return doc;
		}
	}

	/**
	 * Update the audit details for any collection
	 * 
	 * @param doc
	 * @return
	 */
	public static Map<String, Object> updateAudit(Map<String, Object> updateParams) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			AuditEntity audit = new AuditEntity();
			Document authProfile = (Document) (SessionScope.currentUser.get(request.getHeader("X-Access-Token")));
			audit.setLastModifiedDate(new Date());
			if (null != authProfile && authProfile.containsKey("fullName")) {
				updateParams.put("updatedDateTime", audit.getLastModifiedDate());
				updateParams.put("updatedBy", authProfile.get("fullName"));
			}
			return updateParams;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return updateParams;
		}
	}

	/**
	 * Update the audit details for any collection
	 * 
	 * @param doc
	 * @return
	 */
	public static Criteria updateQuery(String collectionName) {
		Criteria cri = new Criteria();
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();

			Document authProfile = (Document) (SessionScope.currentUser.get(request.getHeader("X-Access-Token")));

			if (collectionName.equals("action") || collectionName == "action") {
				if (authProfile.containsKey("profileId"))
					cri = Criteria.where("profileId").is(authProfile.get("profileId"));
				else
					cri = Criteria.where("businessId").is(authProfile.get("businessId"));
			} else if (collectionName.equals("gig") || collectionName == "gig") {
				// allow the update operation to proceed without auth profile if
				// the request contains the Auth Key in its header
				if (null == authProfile && (request.getHeader("Authorization") != null)) {
					// continue
				} else if (null == authProfile.get("userType") && authProfile.containsKey("businessId"))
					cri = Criteria.where("businessId").is(authProfile.get("businessId"));

			}
			return cri;
		} catch (Exception e) {
			return cri;

		}
	}
}
