package com.lifing.syzygy.infra.aspect;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lifing.syzygy.infra.exception.SyzBaseException;
import com.lifing.syzygy.infra.util.SessionScope;

@Aspect
@Component
public class AspectAuthentication {

	@Autowired
	Environment env;

	@Autowired
	MongoTemplate mongoTemplate;

	//@Autowired
	//private EurekaClient eurekaClient;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void requestMapping() {
	}

	@Pointcut("execution(* com.lifing.syzygy..controller.*Controller.*(..))")
	public void methodPointcut() {
	}

	@Pointcut("execution(* com.makepath.lemonop..controller.*Controller.*(..))")
	public void adminMethodPointcut() {
	}

	@Before("requestMapping() && (methodPointcut() || adminMethodPointcut())")
	public Object profile() throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HttpURLConnection connection = null;

		try {
			log.info(request.getHeader("X-Access-Token"));
			if (null == request.getHeader("X-Access-Token") || "" == request.getHeader("X-Access-Token")) {
				SessionScope.currentUser.put("currentUser", null);

			} else if ("" != request.getHeader("X-Access-Token") || null != request.getHeader("X-Access-Token")
					|| !request.getHeader("X-Access-Token").equals(null)) {
				// Create connection


				String urlPath = getRemoteUrl(env.getProperty("api.auth.service"), env.getProperty("api.auth.url"));
				 
				URL url = new URL(urlPath);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestProperty("X-Access-Token", request.getHeader("X-Access-Token"));
				connection.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
				if (!request.getHeader("X-Access-Token").startsWith("ey")) {
					connection.setRequestProperty("from", "employer");
				}
				// Get Response
				InputStream is = connection.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}

				// get the autheticated user details
				Document currentUser = Document.parse(sb.toString());

				// check for authentication
				if (currentUser.containsKey("error"))
					throw new SyzBaseException("CORE-INRFA-0002", new Object[] {});
				if (null != currentUser.get("principal"))
					SessionScope.currentUser.put(request.getHeader("X-Access-Token"), currentUser.get("principal"));
				else
					SessionScope.currentUser.put(request.getHeader("X-Access-Token"), currentUser);

				// for auditing
				/*
				 * try { Document audit = (Document) (currentUser.get("principal"));
				 * audit.put("auditTime", new Date()); audit.put("requestURL",
				 * request.getRequestURL().toString()); audit.put("methodName",
				 * request.getMethod()); audit.put("X-Acess-Token",
				 * request.getHeader("X-Access-Token")); audit.put("queryString",
				 * request.getQueryString()); audit.put("remoteAddress",
				 * request.getRemoteAddr()); audit.put("remoteHost", request.getRemoteHost());
				 * audit.put("createdDate", new Date());
				 * 
				 * if ("POST".equalsIgnoreCase(request.getMethod()) ||
				 * "PUT".equalsIgnoreCase(request.getMethod())) { audit.put("postData",
				 * request.getReader().lines().collect(Collectors.joining(System.lineSeparator()
				 * ))); }
				 * 
				 * mongoTemplate.insert(audit, "auditTrail"); } catch (Exception e) {
				 * log.warn(e.getMessage());
				 * 
				 * }
				 */
			}

		} catch (Exception e) {
			log.error("AspectAuthentication.profile()", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		}
		return connection;
	}

	public String getRemoteUrl(String applicationName, String apiPath) {
		String url = null;
		try {
			if (applicationName.contains("educationhackfest") || applicationName.contains("lemonop")
					|| applicationName.contains("localhost")) {
				url = applicationName + apiPath;
			} /*else {
				Application application = eurekaClient.getApplication(applicationName);
				InstanceInfo instanceInfo = application.getInstances().get(0);
				url = "http://" + instanceInfo.getHostName() + ":" + instanceInfo.getPort() + apiPath;
			}*/
		} catch (Exception e) {

		}
		return url;
	}
}
